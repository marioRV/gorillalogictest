//
//  ViewController.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orderButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let reuseIdentifier = "TestCell"
    private let itemsPerRow: CGFloat = 2
    
    private let sectionInsets = UIEdgeInsets(top: 10.0,
    left: 10.0,
    bottom: 10.0,
    right: 10.0)
    
    var viewModel: IceCreamViewModel
    
    required init?(coder: NSCoder) {
        viewModel = IceCreamViewModel()
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initBinding()
        initCollectionView()
        viewModel.getIceCreams()
    }
    
    private func initBinding() {
        viewModel.cellViewModels.valueChanged = { [weak self] (_) in
            self?.collectionView.reloadData()
        }
        
        viewModel.isLoading.valueChanged = { [weak self] (isLoading) in
            self?.setLoading(isLoading: isLoading)
        }
    }
    
    private func initCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
//        tableView.estimatedRowHeight = 40
//        tableView.rowHeight = UITableView.automaticDimension
        
        collectionView.register(UINib(nibName: IceCreamCollectionViewCell.cellIdentifier(), bundle: nil), forCellWithReuseIdentifier: IceCreamCollectionViewCell.cellIdentifier())
    }
    
    private func setLoading(isLoading: Bool) {
        if isLoading {
            self.activityIndicator.startAnimating()
        }
        else {
            self.activityIndicator.stopAnimating()
        }
    }
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.cellViewModels.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let rowViewModel = viewModel.cellViewModels.value[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier(for: rowViewModel), for: indexPath)
        
        if let cell = cell as? CollectionCellConfigurable {
            cell.setup(viewModel: rowViewModel)
        }
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let rowViewModel = viewModel.cellViewModels.value[indexPath.row]
        
        
    }
    
    func cellIdentifier(for viewModel: CollectionCellViewModel) -> String {
        switch viewModel {
        case is IceCreamCellViewModel:
            return IceCreamCollectionViewCell.cellIdentifier()
        default:
            fatalError("Unexpected view model type: \(viewModel)")
        }
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
      return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return sectionInsets.left
    }
}

