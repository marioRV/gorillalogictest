//
//  IceCreamViewModel.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import Foundation

public class IceCreamViewModel
{
    let cellViewModels = Observable<[IceCreamCellViewModel]>(value: [])
    let repository: IceCreamRepository
    let isLoading = Observable<Bool>(value: false)
    
    init() {
        repository = RemoteIceCreamRepository()
    }
    
    func getIceCreams() {
        repository.fetchIceCreams(complete: {[weak self] (iceCreams) in
                self?.isLoading.value = false
                self?.buildCellViewModels(iceCreams: iceCreams)
            })
    }
    
    private func buildCellViewModels(iceCreams: [IceCream]) {
        var viewModels = [IceCreamCellViewModel]()
        for iceCream in iceCreams {
            let cellViewModel: IceCreamCellViewModel = IceCreamCellViewModel(name1: iceCream.name1, name2: iceCream.name2, price: iceCream.price, bg_color: iceCream.bg_color, type: iceCream.type)
                viewModels.append(cellViewModel)
        }
        
        cellViewModels.value = viewModels
    }
}
