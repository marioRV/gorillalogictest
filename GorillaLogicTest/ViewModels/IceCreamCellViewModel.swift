//
//  IceCreamCellViewModel.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import Foundation

struct IceCreamCellViewModel : CollectionCellViewModel {
    
    let name1: String
    let name2: String
    let price: String
    let bg_color: String
    let type: String
    
    var selectionCount: Int = 0
    
    var cellPressed: (()->Void)?
    
    init(name1: String, name2: String, price: String, bg_color: String, type: String) {
        self.name1 = name1
        self.name2 = name2
        self.price = price
        self.bg_color = bg_color
        self.type = type
    }
    
    mutating func cellWasSelected() {
        self.selectionCount += 1
        
        if(selectionCount == 3)
        {
            selectionCount = 0
        }
    }
}
