//
//  CollectionCellConfigurable.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import Foundation

protocol CollectionCellConfigurable {
    func setup(viewModel: CollectionCellViewModel)
}
