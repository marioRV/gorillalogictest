//
//  IceCreamCollectionViewCell.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import UIKit

class IceCreamCollectionViewCell: UICollectionViewCell, CollectionCellConfigurable {
    
    @IBOutlet weak var selectionCountLabel: UILabel!
    @IBOutlet weak var selectionCountView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var backgroundIconView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    
     var viewModel: IceCreamCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(viewModel: CollectionCellViewModel) {
        guard let viewModel = viewModel as? IceCreamCellViewModel else { return }
        self.viewModel = viewModel
        
        backgroundIconView.backgroundColor = UIColor(hexString: viewModel.bg_color)
        iconImageView.image = UIImage(named: viewModel.type)
        titleLabel.text = viewModel.name1
        priceLabel.text = viewModel.price
        
        backgroundIconView.layer.cornerRadius = backgroundIconView.frame.height / 2
        backgroundIconView.layer.masksToBounds = false
        backgroundIconView.clipsToBounds = true
        
        outerView.isHidden = true
        selectionCountView.isHidden = true
        innerView.layer.borderWidth = 1
    }
}
