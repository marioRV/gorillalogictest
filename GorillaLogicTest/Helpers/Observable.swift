//
//  Observable.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import Foundation

class Observable<T> {
    var value: T {
        didSet {
            DispatchQueue.main.async {
                self.valueChanged?(self.value)
            }
        }
    }
    
    var valueChanged: ((T) -> Void)?
    
    init(value: T) {
        self.value = value
    }
}
