//
//  IceCream.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import Foundation
import Mapper

struct IceCream: Mappable, Decodable {
    let name1: String
    let name2: String
    let price: String
    let bg_color: String
    let type: String
    
    init(map: Mapper) throws {
        name1 = map.optionalFrom("name1") ?? ""
        name2 = map.optionalFrom("name2") ?? ""
        price = map.optionalFrom("price") ?? ""
        bg_color = map.optionalFrom("bg_color") ?? ""
        type = map.optionalFrom("type") ?? ""
    }
}
