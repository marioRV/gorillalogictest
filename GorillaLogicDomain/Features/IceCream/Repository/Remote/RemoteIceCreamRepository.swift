//
//  RemoteIceCreamRepository.swift
//  GorillaLogicTest
//
//  Created by Mario Rúa on 1/19/20.
//  Copyright © 2020 Mario Rúa. All rights reserved.
//

import Foundation

public class RemoteIceCreamRepository : IceCreamRepository
{
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    var errorMessage = ""
    var iceCreams: [IceCream] = []
    
    typealias JSONDictionary = NSDictionary
    typealias JSONArray = [JSONDictionary]
    typealias QueryResult = ([IceCream]?, String) -> Void
    
    func fetchIceCreams(complete: @escaping (([IceCream]) -> Void)) {
        dataTask?.cancel()
        
        if var urlComponents = URLComponents(string: "https://gl-endpoint.herokuapp.com/products") {
        urlComponents.query = ""
        
        guard let url = urlComponents.url else {
          return
        }
            
            dataTask = defaultSession.dataTask(with: url) { [weak self] data, response, error in
                defer {
                  self?.dataTask = nil
                }
                    if let error = error {
                        self?.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                      } else if
                        let data = data,
                        let response = response as? HTTPURLResponse,
                        response.statusCode == 200 {
                        
                        self?.updateSearchResults(data)
                        
                        DispatchQueue.main.async {
                            complete(self!.iceCreams)
                        }
                      }
                    }
                    
                    dataTask?.resume()
            }
    }
    
    private func updateSearchResults(_ data: Data) {
      var response: JSONArray?
      iceCreams.removeAll()
      
      do {
        response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONArray
      } catch let parseError as NSError {
        errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
        return
      }
      
        guard let array = response else {
        errorMessage += "Dictionary does not contain results key\n"
        return
      }
        
      for trackDictionary in array
      {
        iceCreams.append(IceCream.from(trackDictionary)!)
      }
    }
}
